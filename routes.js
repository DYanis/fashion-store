var mdbClient = require('mongodb').MongoClient;

exports.category = function(req, res) {
    mdbClient.connect("mongodb://localhost:27017/store", function(err, db) {
        var id = req.params.category.toLowerCase();
        db.collection('categories', function(err, collection) {
            collection.findOne({
                id: id
            }, function(err, category) {
                if (category) {
                    res.render('categoryView', category);
                    db.close();
                }
            });
        });
    });
};

exports.products = function(req, res) {
    var categoryId = req.params.id;
    mdbClient.connect("mongodb://localhost:27017/store", function(err, db) {
        db.collection('products', function(err, collection) {
            collection.find({
                primary_category_id: categoryId
            }).toArray(function(err, products) {
                if (products.length > 0) {
                    var productsViewModels = [];
                    products.forEach(product => {
                        var productViewModel = {
                            id: product.id,
                            name: product.name,
                            description: product.page_description,
                            price: product.price,
                            currency: product.currency,
                            subcategory: product.primary_category_id,
                            imgLink: product.image_groups[0].images[0].link,
                            imgAlt: product.image_groups[0].images[0].alt,
                            imgTitle: product.image_groups[0].images[0].title,
                        };
                        productsViewModels.push(productViewModel);
                    });

                    res.render('categoryProductsView', {
                        category: req.params.id,
                        products: productsViewModels
                    });
                } else {
                    res.send('Cannot be found products whit this category.');
                }

                db.close();
            });
        });
    });
};

exports.product = function(req, res) {
    var productId = req.params.id;
    mdbClient.connect("mongodb://localhost:27017/store", function(err, db) {
        db.collection('products', function(err, collection) {
            collection.findOne({
                id: productId
            }, function(err, product) {
                if (product) {
                    var productViewModel = {
                        name: product.name,
                        description: product.page_description,
                        price: product.price,
                        currency: product.currency,
                        subcategory: product.primary_category_id,
                        imgLink: product.image_groups[0].images[0].link,
                        imgAlt: product.image_groups[0].images[0].alt,
                        imgTitle: product.image_groups[0].images[0].title,
                    };
                    res.render('productView', productViewModel);
                } else {
                    res.send('Product cannot be found.');
                }

                db.close();
            });
        });
    });
};
